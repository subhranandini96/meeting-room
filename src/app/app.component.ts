import { Component, OnInit, ViewChild } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput, startOfDay } from '@fullcalendar/core';
import { MeetingRoomService } from './meeting-room-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'meeting-room';
  userSelectedDate: any;
  
  bookings: any[] = [];

  headers = {
    bookMeetingRoom: true,
    scheduledMeetingRooms: false
  }
  
  userSelectedData: any;

  constructor(
    private meetingService: MeetingRoomService
  ) {}
  ngOnInit() {}

  dateClick(event) {
    this.userSelectedDate = new Date(event.date);
  }

  addStyles(evt, tabName) {

    switch (tabName) {
      case 'meeting-room':
        this.headers.bookMeetingRoom = true;
        this.headers.scheduledMeetingRooms = false;
        break;
      case 'scheduled':
        this.headers.bookMeetingRoom = false;
        this.headers.scheduledMeetingRooms = true;
        break;
    
      default:
        break;
    }
  }

  eventClick(event) {
    this.userSelectedData = event;
    this.userSelectedDate = event.start;
  }

  isOverlapping(startA, endA, meetingRoom) {
      return this.bookings.some(booking => {
        return startA <= booking.end && endA >= booking.start && meetingRoom === booking.meetingRoom;
      })
    }

  userMeetingDetail(event) {
    if (this.bookings.length > 1 && this.isOverlapping(event.startDate, event.endDate, event.selectedMeetingRoom)) {
      console.log('overlapping');
      return;
    }
    this.bookings.push({
      start: event.startDate,
      end:event.endDate,
      title: event.agenda,
      id: event.userId,
      bookedBy: event.id,
      meetingRoom: event.selectedMeetingRoom
    });

    const room = this.meetingService.meetingRooms.find((room) => room.name === event.selectedMeetingRoom);
    if (room) {
      room.scheduleDetails.push(event);    
    }      
  }

} 



