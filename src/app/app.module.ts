import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CalenderComponent } from './calender/calender.component';
import { BookingFormComponent } from './booking-form/booking-form.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { ScheduledMeetingRoomComponent } from './scheduled-meeting-room/scheduled-meeting-room.component';
import { MeetingRoomService } from './meeting-room-service';

@NgModule({
  declarations: [
    AppComponent,
    CalenderComponent,
    BookingFormComponent,
    ScheduledMeetingRoomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FullCalendarModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    MeetingRoomService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
