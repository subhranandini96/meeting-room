import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MeetingRoomService } from '../meeting-room-service';

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.scss']
})
export class BookingFormComponent implements OnInit, OnChanges {

  @Input() selectedDate = new Date();
  @Input() userSelectedData: any;
  meetingRooms = [{name: 'qwe'}, {name: 'www'}, {name: 'eee'}, {name: 'ttt'}, {name: 'aaa'}];

  @Output() meetingDetail:  EventEmitter<any> = new EventEmitter();

  startTimeInMinutes: number;
  endTimeInMintues: number;

  startDate = new Date();

  meetingRoomForm: FormGroup;
  userIdDetails = [];

  exceedingOfficeHour = false;
  beforeOfficeHour = false;
  durationGreaterThanThrity = false;

  get disableSubmitButton() {
    return this.meetingRoomForm.invalid || this.exceedingOfficeHour ||
    this.beforeOfficeHour ||
    this.durationGreaterThanThrity
  }

  constructor(
    private formBuilder: FormBuilder,
    private meetingService: MeetingRoomService
    ) { }

  ngOnInit() {    
    this.meetingRoomForm = this.formBuilder.group({
      userId: [null, Validators.required],
      selectedMeetingRoom: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      meetingDate: [null, Validators.required],
      agenda: ['', Validators.required]
    });

    this.meetingRooms = this.meetingService.meetingRooms;

    for (let i=0; i<10; i++) {
      this.userIdDetails.push(Math.random());
    }

    this.meetingRoomForm.get('startTime').valueChanges.subscribe((val) => {
      const startTime = Number(val.split(':')[0]);
      if (startTime > 9) {
        this.changeToMinute('start');
        this.beforeOfficeHour = false;
      } else {
        this.beforeOfficeHour = true;
        alert('We start working from 9am.');
      }
    });

    this.meetingRoomForm.get('endTime').valueChanges.subscribe((val) => {
      let endTime = Number(val.split(':')[0]);
      if (endTime >= 1 && endTime <=6) {
        endTime = endTime+12;
        const time = endTime.toString() + ":" + val.split(":")[1];
        this.meetingRoomForm.get('endTime').setValue(time);
      }
      if (endTime <= 18) {
        this.changeToMinute('end');
        this.exceedingOfficeHour = false;
      } else {
        this.exceedingOfficeHour = true;
        alert('exceeding ofc hour');
      }
    });

    this.meetingRoomForm.get('meetingDate').valueChanges.subscribe((val) => {
      let selectedDate = this.meetingRoomForm.get('meetingDate').value;
      if (new Date(selectedDate).getDay() === 0 || new Date(selectedDate).getDay() === 6) {
        alert('We are working from Mon-Sat');
        this.meetingRoomForm.get('meetingDate').setValue(null);
        return;
      }
      this.checkValidDate();
    });
  }

  ngOnChanges(simpleChange: SimpleChanges) {
    if (simpleChange.selectedDate && simpleChange.selectedDate.currentValue) {
      this.startDate = simpleChange.selectedDate.currentValue;
      const val = new Date(this.startDate);
      const year = val.getFullYear();
      let month: any = val.getMonth()+1;
      if (month < 10) {
        month = "0" + month;
      }
      let day: any = val.getDate();
      if (day < 10) {
        day = "0" + day;
      }
      const date = year + '-' + month + '-' + day;
      this.meetingRoomForm.get('meetingDate').setValue(date);
    }

    if (simpleChange.userSelectedData && simpleChange.userSelectedData.currentValue) {
      const data = simpleChange.userSelectedData.currentValue;
      this.meetingRoomForm.get('userId').setValue(data.userId);
      this.meetingRoomForm.get('startTime').setValue(data.startTime);
      this.meetingRoomForm.get('endTime').setValue(data.endTime);
      this.meetingRoomForm.get('selectedMeetingRoom').setValue(data.selectedMeetingRoom);
    }
  }

  checkValidDate() {
    const startDate = this.meetingRoomForm.get('meetingDate').value;
    if (new Date(startDate) < new Date()) {
      alert('Sorry!! Youcant create meeting before today date');
      return;
    }
  }

  changeToMinute(forWhich: string) {
    const startTime = this.meetingRoomForm.get('startTime').value;
    const endTime = this.meetingRoomForm.get('endTime').value;
    switch (forWhich) {
      case 'start':
        this.startTimeInMinutes = Number(startTime.split(":")[0] * 60) + Number(startTime.split(":")[1]);
        break;
      case 'end':
        this.endTimeInMintues = Number(endTime.split(":")[0] * 60) + Number(endTime.split(":")[1]);
        break;    
      default:
        break;
    }

    if (forWhich === 'end' && !this.isDurationThirtyMinutes()) {
      this.durationGreaterThanThrity = true;
      alert('meeting duration shoule be greater than equal to 30 minutes');
    } else {
      this.durationGreaterThanThrity = false;
    }
  }

  isDurationThirtyMinutes() {
    if (this.startTimeInMinutes) {
      return ((this.endTimeInMintues - this.startTimeInMinutes) >= 30)
    }
  }

  submit() {
    const startDate = this.meetingRoomForm.get('meetingDate').value;
    const startTime = this.meetingRoomForm.get('startTime').value;

    const endDate = this.meetingRoomForm.get('meetingDate').value;
    const endTime = this.meetingRoomForm.get('endTime').value;
    const data = {
      ...this.meetingRoomForm.value,
      startDate: new Date(startDate + ":" + startTime),
      endDate: new Date(endDate + ":" + endTime)
    };
    
    if (this.disableSubmitButton) {
      alert('Please fill all the details');
      return;
    }
    
    this.meetingDetail.emit(data);
    alert('Data submitted Successfully');
    this.meetingRoomForm.reset();
  }

}
