import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';

import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
import dayGridPlugin from '@fullcalendar/daygrid';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})
export class CalenderComponent implements OnInit {

  @Input() events = [];
  @Output() eventClick: EventEmitter<any> = new EventEmitter();
  @Output() dateClick: EventEmitter<any> = new EventEmitter();

  title = 'meeting-room';

  @ViewChild('calendar', null) calendarComponent: FullCalendarComponent; // the #calendar in the template

  constructor() { }

  ngOnInit() { }
  
  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];
  calendarWeekends = false;

  handleDateClick(arg) {
    if (arg.date < new Date()) {
      alert('Sorry!! Youcant create meeting before today date');
      return;
    }
    this.dateClick.emit(arg);
  }

  handleEventClick(event) {
    const currentEvent = event.event;
    const data = {
      startTime: (new Date(currentEvent.start).getHours()).toString() + ":" + (new Date(currentEvent.start).getMinutes()).toString(),
      endTime: (new Date(currentEvent.start).getHours()).toString() + ":" + (new Date(currentEvent.start).getMinutes()).toString(),
      userId: currentEvent.id,
      meetingRoom: currentEvent.extendedProps.meetingRoom,
      start: currentEvent.start,
      agenda: currentEvent.title
    };
    this.eventClick.emit(data);
  }

}
