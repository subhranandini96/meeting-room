import { TestBed } from '@angular/core/testing';

import { MeetingRoomServiceService } from './meeting-room-service.service';

describe('MeetingRoomServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MeetingRoomServiceService = TestBed.get(MeetingRoomServiceService);
    expect(service).toBeTruthy();
  });
});
