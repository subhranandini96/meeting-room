import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MeetingRoomService {

  constructor() { }
  meetingRooms = [
    { name: 'Albert Einstein', scheduleDetails: [] },
    { name: 'Isaac Newton', scheduleDetails: [] },
    { name: 'Marie Curie', scheduleDetails: [] },
    { name: 'Galileo Galilei', scheduleDetails: [] },
    { name: 'Charles Darwin', scheduleDetails: [] },
    { name: 'Nikola Tesla', scheduleDetails: [] },
    { name: 'Thomas Edison', scheduleDetails: [] },
    { name: 'Stephen Hawking', scheduleDetails: [] },
    { name: 'Michael Faraday', scheduleDetails: [] },
    { name: 'James Maxwell', scheduleDetails: [] }
  ]

  meetingRoomMap = {};
}
