import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduledMeetingRoomComponent } from './scheduled-meeting-room.component';

describe('ScheduledMeetingRoomComponent', () => {
  let component: ScheduledMeetingRoomComponent;
  let fixture: ComponentFixture<ScheduledMeetingRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduledMeetingRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduledMeetingRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
