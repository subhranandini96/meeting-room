import { Component, OnInit } from '@angular/core';
import { MeetingRoomService } from '../meeting-room-service';

@Component({
  selector: 'app-scheduled-meeting-room',
  templateUrl: './scheduled-meeting-room.component.html',
  styleUrls: ['./scheduled-meeting-room.component.scss']
})
export class ScheduledMeetingRoomComponent implements OnInit {

  meetingRoomDetails = [];
  meetinfDataToBeShown: any;
  sheduledMeetingRoomDetails: any;

  constructor(
    private meetingRoomService: MeetingRoomService
  ) { }

  ngOnInit() {
    // console.log('scheduled meeting room details', this.meetingRoomService.meetingRooms);

    this.meetingRoomDetails = this.meetingRoomService.meetingRooms;

  }
  roomDetails(event) {
    this.sheduledMeetingRoomDetails = this.meetingRoomDetails.find((room) => room.name === this.meetinfDataToBeShown)
  }

  deleteMeeting(data, index) {
    const room = this.meetingRoomDetails.find((room) => data.selectedMeetingRoom === room.name );
    room.scheduleDetails = room.scheduleDetails.filter((room, roomIndex) => {
      return roomIndex !== index
    });
    console.log(this.meetingRoomDetails);
    
  }
    

}
